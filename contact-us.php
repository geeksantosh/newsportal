<?php
include('includes/config.php');

?>
<!DOCTYPE html>
<html lang="en">

  <head>

   <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="ElightTechnical | New way to Learn" content="TECHNOLOGY UPDATES, SEO, TIPS, TRICKS, SECURITY UPDATES, REVIEWS ALL ABOUT TECHNOLOGY ">
    <meta name="author" content="Santosh Adhikari">

    <title>ElightTechnical | Contact us</title>

 
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

 
    <link href="css/modern-business.css" rel="stylesheet">
    <style type="text/css">
      body {
    text-align: center;
}
form {
    display: inline-block;
}

.form-control{
   padding: 1.5px auto;
   margin: 5px;

}

    </style>
  </head>

  <body>

    <?php include('includes/header.php');?>
   
      
<br><br>
<form name="htmlform" method="post" action="html_form_send.php" >
   <div class="form-group">
<table width="450px">
</tr>

<tr>
 <td valign="top">
  <label for="first_name">First Name *</label>
 </td>
 <td valign="top">
  <input  type="text" name="first_name" maxlength="50" size="30" class="form-control" required="">
 </td>
</tr>
 
<tr>
 <td valign="top"">
  <label for="last_name">Last Name *</label>
 </td>
 <td valign="top">
  <input  type="text" name="last_name" maxlength="50" size="30" class="form-control" required="">
 </td>
</tr>
<tr>
 <td valign="top">
  <label for="email">Email Address *</label>
 </td>
 <td valign="top">
  <input  type="email" name="email" maxlength="80" size="30" class="form-control" required="">
 </td>
 
</tr>
<tr>
 <td valign="top">
  <label for="telephone">Contact Number*</label>
 </td>
 <td valign="top">
  <input  type="Number" name="telephone" maxlength="30" size="30" class="form-control" required="">
 </td>
</tr>
<tr>
 <td valign="top">
  <label for="telephone">Website*</label>
 </td>
 <td valign="top">
  <input  type="text" name="link" maxlength="30" size="30" class="form-control" required="">
 </td>
</tr>
<tr>
 <td valign="top">
  <label for="comments">Comments *</label>
 </td>
 <td valign="top">
  <textarea  name="comments" maxlength="1000" rows="4" id="comment" class="form-control" required=""></textarea>
 </td>
 
</tr>
<tr>
 <td colspan="2" style="text-align:center">
 <br> <input type="submit" value="Submit" id="form1" class="btn btn-primary">   
 </td>
</tr>
</table>
</div>
</form>



     
 <?php include('includes/footer.php');?>


    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>
